#!/bin/bash -ex

HOSTNAME_FQDN=$1

TMP=`mktemp -d /tmp/XXXXXXXXX`
cp -r etc ${TMP}
grep -rl '${HOSTNAME_FQDN}' ${TMP} | xargs sed -i "s/\${HOSTNAME_FQDN}/${HOSTNAME_FQDN}/g"
grep -rl '${MARIADB_PWD}' ${TMP} | xargs sed -i "s/\${MARIADB_PWD}/${MARIADB_PWD}/g"
grep -rl '${REDIS_PWD}' ${TMP} | xargs sed -i "s/\${REDIS_PWD}/${REDIS_PWD}/g"

if [[ ${HOSTNAME_FQDN} == *test.* ]] ; then
	BANNER="
		<div style='background: red; color: yellow; text-align: center; font-weight: 900;'>
			WARNING: THIS IS A TEST INSTANCE. DATA CAN VANISH AT ANY TIME.
		</div>"
	echo "${BANNER}" > ${TMP}/etc/gitea/templates/custom/body_outer_pre.tmpl
	echo "Disallow: /" > ${TMP}/etc/gitea/public/robots.txt
fi

rsync -av -e ssh --chown=root:root ${TMP}/etc root@${HOSTNAME_FQDN}:/
rsync -av -e ssh --delete --chown=git:git ${TMP}/etc/gitea root@${HOSTNAME_FQDN}:/etc/
rsync -av -e ssh --delete --chown=git:git ${TMP}/etc/gitea/public root@${HOSTNAME_FQDN}:/etc/gitea/
rm -rf ${TMP}

